'use strict'
const storage = require('node-persist')

storage.initSync()


//Validate - Ensure that there is data being provided by checking if recipe.name exists
exports.checkrecipe = function(recipe) {
	if (recipe.name === undefined) return false
	return true
}


//Get List - Return the whole list of favourites by checking that the length of the list is more than 0
exports.countRecipes = function() {
	const favourites = storage.values()

	console.log(`there are ${favourites.length} indexes in the array`)
	if (favourites.length) return true
	return false
}


//Post - add a recipe to favourites by checking that the recipe being added isnt undefined
exports.addrecipe = function(recipe) {
	if (storage.getItemSync(recipe.name) !== undefined) {
		return false
	}
  //This adds the data.
	storage.setItemSync(recipe.name, recipe)
	return true
}


//Get - get a single recipe from favourites by checking that the recipe asked for is defined
exports.getRecipe =function(recipe){
	const recipename = storage.getItemSync(recipe.name)

	if (recipe === undefined) throw new Error(`recipe "${recipename}" not found`)
	return recipe
}


//Put - update a favourite by checking that the recipe exists
exports.updateRecipe = function(recipe) {
	if (storage.getItemSync(recipe.name) === undefined) {
		return false
	}
	storage.setItemSync(recipe.name, recipe)
	return true
}


//Delete - delete a favourite by checking that the recipe exists
exports.deleterecipe =function(recipe){
	if (storage.getItemSync(recipe.name) === undefined) {
		return false
	}
	storage.removeItem(recipe.name)
	return true
}
