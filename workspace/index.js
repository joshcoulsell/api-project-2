'use strict'

//initalise server using the restify package
const restify = require('restify')
const server = restify.createServer()
const defaultPort = 8080

const edamam = require('./cooking')
const favourites = require('./favourites')

server.use(restify.fullResponse())
server.use(restify.queryParser())
server.use(restify.bodyParser())
server.use(restify.authorizationParser())

server.get('/cooking', edamam.recipelookup)

server.get('/favourites', favourites.listFavourites)  // get a list of all favs
server.post('/favourites', favourites.validateFavourite, favourites.addFavourite)  // add a new fav
server.get('/favourites/:name', favourites.getFavourite)  // get details of a particular fav using id
server.put('/favourites', favourites.validateFavourite, favourites.updateFavourite)  // update details of existing fav using id
server.del('/favourites', favourites.deleteFavourite)  // delete existing fav using id


//set the port for the api to run on
const port = process.env.PORT || defaultPort
//display the port in the console log

server.listen(port, err => console.log(err || `App running on port ${port}`))
