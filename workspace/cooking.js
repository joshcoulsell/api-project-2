'use strict'

const request = require('request')
//create a VARIABLE to lookup the recipes from the third party api

const status = {
	'ok': 200,
}

exports.recipelookup = function(req, res){
	const lookup = req.params.q
	const url = `https://api.edamam.com/search?q=${lookup}&app_id=a4717091&app_key=83ccd27ec06f1d291812614c27c11cc9`

	console.log(url)

	//return data from the thrird party
	request.get(url, function(err, resp, body){
		const json = JSON.parse(body)
		//console.log(json.hits[0], null, 2)

		if(!err && resp.statusCode === status.ok) {
			console.log(json.hits[0].recipe.label)
			const recipes = []

			for( let i = 0; i< json.hits.length; i++) {
				console.log(json.hits[i].recipe.label)
				const recipe = {
					name: json.hits[i].recipe.label,								//return label
					ingredients: json.hits[i].recipe.ingredients,		//return ingredients
					Health: json.hits[i].recipe.healthLabels,				//return health lables
				}

				recipes.push(recipe)
			}

			res.send({recipes: recipes})
		}
	})
}
