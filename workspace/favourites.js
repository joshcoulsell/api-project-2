'use strict'
const testing = require('./testing')
const storage = require('node-persist')

storage.initSync()

const status = {
	'cantComplete': 400,
	'created': 201,
	'notFound': 404
}

//Validate - Ensure that there is data being provided
exports.validateFavourite = function validateFavourite(req, res, next) {
	if (testing.checkrecipe(req.body)) next()
	res.send(status.notFound, 'There is no recipe to add')
	res.end()
}
//Get List - Return the whole list of favourites
exports.listFavourites = function listFavourites(req, res) {
	if (testing.countRecipes) {
		res.send(storage.values())
		res.end()
	} else {
		res.send(status.notFound, 'No Favourites list')
		res.end()
	}
}
//Post - add a recipe to favourites
exports.addFavourite = function addFavourite(req, res) {
	if (testing.addrecipe(req.body)) {
		res.send(status.created,'added to favourites')
		res.end()
	} else {
		res.send(status.cantComplete, 'item already exists in favourites')
		res.end()
	}
}
//Get - get a single recipe from favourites
exports.getFavourite = function getFavourite(req, res) {
	if (testing.getRecipe(req.params.name)) {
		res.send(testing.recipe)
		res.end()
	} else {
		res.send(status.cantComplete, 'favourite not found')
		res.end()
	}
}
//Put - update a favourite
exports.updateFavourite = function updateFavourite(req, res) {
	if (testing.updateRecipe(req.body)) {
		res.send(status.created,'updated favourites')
		res.end()
	} else {
		res.send(status.cantComplete, 'cannot find this favourite in the list')
		res.end()
	}
}
//Delete - delete a favourite
exports.deleteFavourite = function deleteFavourite(req, res) {
	if (testing.deleterecipe(req.body)) {
		res.send('Recipe deleted')
		res.end()
	} else {
		res.send(status.cantComplete, 'cannot find this favourite in the list')
		res.end()
	}
}
